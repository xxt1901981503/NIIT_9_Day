package com.xxt.day2;

//抽象类abstract（访问修饰符）
//修复格式：Ctrl + Shift + F
//包含抽象方法的类叫做抽象类，抽象类既可以包含抽象方法，也可以包含普通方法
//final的变量值不可更改，final的方法不能被重写，final的类不能被继承
abstract class Animal{

    String color;
    int leg;

    public void sleep(){
        System.out.println("Animal can sleep");
    }

    //未实现的方法叫做抽象方法，只有方法头部，没有body部分的方法
    public abstract void eat(); //方法头部head
}

//子类继承自抽象类的话，那么必须实现继承的抽象方法
class Dog extends Animal{
    //重写继承的抽象方法，叫做实现该方法
    @Override
    public void eat(){
        System.out.println("狗吃肉");
    }

    @Override
    public void sleep(){
        System.out.println("狗躺着睡觉");
    }

    public void bite(){
        System.out.println("狗会咬人");
    }
}

class Pig extends Animal{

	@Override
	public void eat() {
        System.out.println("猪吃饲料");
	}

    @Override
    public void sleep(){
        System.out.println("猪sleep a lot");
    }
    
}

public class Demo9_Inheritance2 {
	public static void main(String[] args) {
		//Animal a = new Animal(); //抽象类不能实例化，不能创建对象
        Dog dog = new Dog();
        dog.eat();

        Pig pig = new Pig();
        pig.eat();

        Animal animal; //引用变量
        animal = dog;  //animal引用dog
        animal.eat(); //父类的变量可以引用子类对象
        //animal.bite(); //animal虽然引用的是Dog对象，但是自身还是Anima类型，所以不认识bite方法
        //父类变量引用子类对象，那么他只能引用重写过的方法

        animal = pig;
        animal.eat();
	}
}
