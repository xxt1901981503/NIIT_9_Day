package com.xxt.day2;

import java.util.Date;

public class Demo3_Constructs {
	public static void main(String[] args) {
		
		//条件结构
		//1) if...else
        int age = 19;

        if(age >= 18){
            System.out.println("成年人");
        }
        
        if(age >= 18){
            System.out.println("成年人");
        } else {
            System.out.println("未成年人");
            System.out.println("over");
        }

        if(age < 10){
            System.out.println("儿童");
        } else if(age < 18){
            System.out.println("未成年人");
        } else {
            System.out.println("成年人");
        }

        //2) switch...case
        //当判断的结果比较多的时候，请使用switch case
        //导入的快捷键Ctrl + Shift + O
        Date date = new Date();
        int weekday = date.getDay(); //0 - 6
        
        switch(weekday){
            case 1: System.out.println("Monday"); break;
            case 2: System.out.println("Tuesday"); break;
            case 3: System.out.println("Wednesday"); break;
            case 4: System.out.println("Thursday"); break;
            case 5: System.out.println("Friday"); break;
            default: System.out.println("Weekend"); //其他情况
            
        }
        //循环结构
	}
}
