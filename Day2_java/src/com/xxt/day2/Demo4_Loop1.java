package com.xxt.day2;

public class Demo4_Loop1 {
	public static void main(String[] args) {

		//循环结构:重复执行同一个任务，精简代码
		//while,先判断再执行
		
        int i = 0;
        //ctrl + d删除当前行或删除选中的多行
        while(i < 5){
            System.out.println("下次不要忘了主方法");
            i++;
        }

        //i = 5
        
		//do...while，先执行，再判断，至少执行一次
        
        int j = 0;
        do{ //0 1 2 3 4
            System.out.println("我很方");
            j++;
        } while(j < 5);
        
		//for:一般知道确切的循环次数
        //for(变量初始化；循环条件；变量的自增、自减)
        //for(1; 2; 3)
        for(int k = 0; k < 5; k++){
            System.out.println("一会就可以吃饭了");
        }

        //1->2->4->3->2->4->3->2->4->3...
        //要求不断地输入数字，直到输入满50个偶数为止do while,while
        
	}
}
