package com.xxt.day2;

public class Demo5_Loop2 {
	public static void main(String[] args) {
		
		//loop拓展
		
        for(int j = 0; j < 6; j++){ //外循环 控制行
            for(int i = 0; i < j + 1; i++) //内循环 控制列
			    System.out.print("*");
            System.out.println();
        }
         
        System.out.println("------------------");

        for(int i = 0; i < 6; i++){         //外循环控制打印6行
            for(int j = 0; j < 5 -i; j++){  //连续打印5-i个空格{
                System.out.print(" ");
            }

            for(int k = 0; k < 2 * i + 1; k++){ //连续打印i+1个星号
                System.out.print("*");
            }
            System.out.println();
        }

        System.out.println("------------------");
        
        for(int i = 1; i <= 10; i++){
            if(i % 2 == 0) {
                continue;   //continue跳过本次循环,直接执行下一次循环
            }
            System.out.print(i + " ");
        }

        System.out.println("\n------------------");
        
        for(int i = 1; i <= 10; i++){
            if(i % 2 == 0) {
                break;   //break跳过本次循环
            }
            System.out.print(i + " ");
        }
	}
}
