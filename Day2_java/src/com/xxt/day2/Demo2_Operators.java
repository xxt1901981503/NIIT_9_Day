package com.xxt.day2;

public class Demo2_Operators {
	public static void main(String[] args) {
		
		int a = 3;
		if(a % 2 == 0){
			System.out.println("偶数");
		}
		
		//a++ 后缀自增 先使用a，再自加 ++a前缀自增，先自加，再使用
		int b = 1;
		System.out.println(b++);	//1 b = 2
		System.out.println(++b);	//3 b = 3
		System.out.println(b--);	//3 b = 2
		System.out.println(--b);	//1 b = 1
		
		int year = 1998;
		if((year % 400 == 0) || (year % 4 == 0 && year % 100 != 0)){
			System.out.println("闰年");
		}

        //三元运算符
        int c = 10, d = 20;
        if(c > d){
            System.out.println(c);
        } else {
            System.out.println(d);
        }

        //表达式 ? 值1 : 值2
        System.out.println(c > d ? c : d);
	}
}
