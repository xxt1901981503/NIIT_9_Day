package com.xxt.day2;

public class Demo7_Array2 {
	public static void main(String[] args) {
		
	    //二维数组:数组的数组
		int a[][] = new int[2][3];
        a[0][0] = 10;
        a[0][1] = 20;
        a[1][1] = 30;
        a[1][2] = 40;

        System.out.println(a[1][1]);

        //可以定义数组的时候直接初始化
        int b[][] = {
            {1,2, 3,},
            {4, 5, 6}, 
            {7, 8}, 
            {10, 11, 12}
        };
        System.out.println(b[2][1]);

        System.out.println(b.length);   //返回数组的行数
        System.out.println(b[2].length); //第三行的长度
        //遍历二维数组
        for(int i = 0; i < b.length; i++){
            for(int j = 0; j < b[i].length; j++){
                System.out.print(b[i][j] + " ");
            }
            System.out.println();
        }

        String provinces[] = {"山东", "海南", "江苏"};
        String cities[][] = {{"青岛", "济南", "临沂"}, {"海口", "三亚"}, {"无锡", "南京", "常州", "苏州"}};
        //山东：青岛 济南 临沂
		//海南：海口 三亚
		//江苏：无锡 南京 常州 苏州
        for(int i = 0; i < provinces.length; i++){
            System.out.print(provinces[i] + ":");

            for(int j = 0; j < cities[i].length; j++){
                System.out.print(cities[i][j] + " ");
            }

            System.out.println();
        }
	}
}
