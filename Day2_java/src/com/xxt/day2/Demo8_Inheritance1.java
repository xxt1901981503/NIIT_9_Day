package com.xxt.day2;

class Person{

    String name;
    char gender;

    public Person(){
        System.out.println("Person构造函数1");
    }

    public Person(String name, char gender){
        this.name = name;
        this.gender = gender;
        System.out.println("Person构造函数2");
    }

    public void display(){
        System.out.println("姓名：" + name);
        System.out.println("性别：" + gender);
    }
}
//使用extends关键字实现类的继承，extends后面为父类（超类），前面是子类（派生类）
//extends扩展，子类是对父类的扩展，继承实现代码的重复利用，保持程序的结构
//父类的private成员是不会被继承的

class Employee extends Person{
    String title;
    int salary;
    
    public Employee(){
        System.out.println("Employee构造函数1");
    }

    public Employee(String name, char gender, String title, int salary){
        super(name, gender);
        this.title = title;
        this.salary = salary;
        System.out.println("Employee构造函数2");
    }

    @Override //注解
    public void display(){ //方法覆盖，方法重写，重写父类继承的方法
        super.display();    //super关键字只能在子类使用，在子类里调用父类的成员
        System.out.println("职位：" + title);
        System.out.println("工资" + salary);
    }
    
}

public class Demo8_Inheritance1 {
		
	public static void main(String[] args) {
        //调用子类构造函数的时候，它会自动先调用父类的构造函数
	    Employee emp = new Employee("李柏杨", 'F', "CEO", 100);
        //emp.display();
	}
}
