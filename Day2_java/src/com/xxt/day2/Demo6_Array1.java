package com.xxt.day2;

public class Demo6_Array1 {
	public static void main(String[] args) {
		
        //数组（Array）：存储相同该类型的一组数值，存在相邻的内存空间
        int a[] = new int[10];

        int[] b;
        b = new int[5];

        b[0] = 10; //数组名[小标]
        b[1] = 60;
        b[2] = 30;
        b[3] = 40;
        b[4] = 50;

        System.out.println(b[3]);
        //System.out.println(b[5]);
        
        String countries[] = {"China", "Pakistan", "Korea", "USA"};
        System.out.println(countries.length);   //数组的长度
        System.out.println(countries[3]);

        //遍历数组
        for(int i = 0; i < countries.length; i++){
            System.out.print(countries[i] + " ");
        }

        System.out.println();

        //foreach循环，一般用于操作数组或集合
        for(String country:countries){
            System.out.print(country + " ");
        }

        System.out.println();

        for(int x:b){
            System.out.print(x + " ");
        }
	}
}
