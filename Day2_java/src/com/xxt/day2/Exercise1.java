package com.anna.day2;

public class Exercise1 {

    public static void sayHi(String name){
        System.out.println("Hi,"+name);
    }

    public static void main(String args[]){
        sayHi("Allen");
    }
}