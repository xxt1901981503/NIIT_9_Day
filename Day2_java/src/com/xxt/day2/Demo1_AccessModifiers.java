package com.xxt.day2;

//Access Modifiers 访问修饰符：说明类成员如何被使用
//static（静态的），final（最终的），abstract（抽象的）

public class Circle {

	//final修饰的变量只能初始化一次，后期不可改其值
	static double PI = 3.14;	//静态成员,属于类的，所有的对象共享同一个值
	double radius;	//非静态成员，属于对象的，每个对象的该成员不同

	public Circle(double r) {
		radius = r;
	}

	public static void displayPI() {
		System.out.println(PI);
		//System.out.println(radius);
		//displayPI是静态方法，不创建对象就可访问
		//但是radius是非静态成员，同一个类里，静态成员是不能直接访问非静态成员
	}

	public void calArea() {
		System.out.println(PI * radius * radius);
	}
}

public class Demo1_AccessModifiers {
	public static void main(String[] args) {

		Circle c1 = new Circle(2);
		System.out.println(c1.PI);
		System.out.println(c1.radius);

		System.out.println(Circle.PI); //静态成员可以直接通过类名调用，不需要创建对象便可调用
		Circle.displayPI();
		//非静态成员一定要创建对象才能访问

		Circle c2 = new Circle(3);
		Circle c3 = new Circle(4);

		System.out.println("--------------------");

		System.out.println(c2.PI);
		System.out.println(c3.PI);

		//c2.PI = 3.1415; //静态成员是属于类的，所以该类的对象共享同一个值

		System.out.println(c2.PI);
		System.out.println(c3.PI);
	}
}


