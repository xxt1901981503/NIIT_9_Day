package com.xxt.android_day3_listview;

import java.util.ArrayList;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	//MVC(Model-View-Controller 模型-视图-控制器
	
	//ListView显示数据，适配器Adapter：连接视图和数据，数组或List：存放和操作数据
	//Adapter：ArrayAdapter，SimpleAdapter（自定义显示），BaseAdapter（扩展性最好）
	
	ListView lv;
	ArrayList<String> friendList;
	ArrayAdapter<String> adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		lv = (ListView)findViewById(R.id.main_listview1);
		
		friendList = getData();
		
		//第一个参数：一个上下文对象，
		//第二个参数：使用的layout资源的id
		//第三个参数：要关联的数据列表或数组
		adapter = new ArrayAdapter<String>(
				this, 
				android.R.layout.simple_list_item_1, 
				friendList);
		lv.setAdapter(adapter);
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String data = friendList.get((int)id);
				//id被点击的行的行号，从0开始
				//列表的get方法根据索引号返回对应位置的数据
				Toast.makeText(MainActivity.this, "您选择了：" + data, Toast.LENGTH_SHORT).show();
			}
		});
		
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				
				friendList.remove((int)id);//从数据源，也就是列表删除长按的那项的数据
				adapter.notifyDataSetChanged();//通知数据集更改，刷新ListView视图内容
				Toast.makeText(MainActivity.this, "删除成功", Toast.LENGTH_SHORT).show();
				
				return true;//返回true说明已经处理完事件，不需要继续传播
			}
		});
	}
	
	//自定义一个方法，返回一个放好数据的列表
	public ArrayList<String> getData(){
		ArrayList<String> list = new ArrayList<String>();
		list.add("李凡凡");
		list.add("杨婉清");
		list.add("李奕辰");
		list.add("阿力");
		
		return list;
	}
	
}
