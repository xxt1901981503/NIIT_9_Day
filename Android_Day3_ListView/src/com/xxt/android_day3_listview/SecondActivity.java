package com.xxt.android_day3_listview;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class SecondActivity extends Activity{
	
	ListView lv;
	ArrayList<HashMap<String,Object>> friendList;
	SimpleAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
		lv = (ListView)findViewById(R.id.second_listView1);
		friendList = getData();
		//第一个参数：使用适配器的上下文对象
		//第二个参数：你要使用的数据列表
		//第三个参数：指定列表视图每一项的样式布局文件的id
		//第四个参数：一个字符串数组，给出我们Map集合中各个数据的键名称
		//第五个参数：一个整数数组，给出集合中的数据映射好哪个视图上，给出视图的id
		String[] from = {"image", "nickname", "motto"};
		int[] to = {R.id.item_image, R.id.item_nickname, R.id.item_motto};
		adapter = new SimpleAdapter(this, friendList, R.layout.layout_item, from, to);
		lv.setAdapter(adapter);
		
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				//long id传入的是被点击的那一行的行号，从0开始
				HashMap<String, Object> friend = friendList.get((int) id);
				String name = friend.get("nickname").toString();
				Toast.makeText(SecondActivity.this, "你选择了：" + name, Toast.LENGTH_SHORT).show();
			}
		});
		
		//要求长按某一项，删除该项
		lv.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
				
				friendList.remove((int)id);
				adapter.notifyDataSetChanged();
				
				return true;
			}
		});
		
	}
	
	//返回一个列表，返回
	public ArrayList<HashMap<String,Object>> getData(){
		
		ArrayList<HashMap<String,Object>> list = new ArrayList<HashMap<String,Object>>();
		
		HashMap<String,Object> hm1=new HashMap<String,Object>();
		hm1.put("image", R.drawable.p1);//Integer
		hm1.put("nickname", "李凡凡");//String
		hm1.put("motto", "天大地大，吃饭最大");//String
		list.add(hm1);
		
		HashMap<String,Object> hm2=new HashMap<String,Object>();
		hm2.put("image", R.drawable.p2);
		hm2.put("nickname", "阿里");
		hm2.put("motto", "我最帅");
		list.add(hm2);
		
		HashMap<String,Object> hm3=new HashMap<String,Object>();
		hm3.put("image", R.drawable.p3);
		hm3.put("nickname", "李博洋");
		hm3.put("motto", "他也不知道");
		list.add(hm3);
		
		return list;
	}

}
