package com.xxt.android_day5_broadcastreceiver1;

import com.xxt.android_day5_broadcastreciever1.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
	
	Button btn_send;
	EditText et_msg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		btn_send = (Button)findViewById(R.id.btn_send);
		et_msg = (EditText)findViewById(R.id.et_msg);
		
		btn_send.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent intent = new Intent(MainActivity.this,MessageReceiver.class);
				
				Bundle bundle = new Bundle();
				bundle.putString("name", "Anna");
				bundle.putString("msg", et_msg.getText().toString());
				intent.putExtras(bundle);
				
				sendBroadcast(intent);//���͹㲥
			}
		});
	}

	
}
