package com.xxt.android_day5_broadcastreceiver1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class MessageReceiver extends BroadcastReceiver{

	//onReceive响应接收到广播执行
	@Override
	public void onReceive(Context context, Intent intent) {
		Bundle bundle = intent.getExtras();//获取Intent里面的数据包
		String name = bundle.getString("name");//根据键获取对应的值
		String msg = bundle.getString("msg");
		Toast.makeText(context, name + "，收到消息内容：" + msg + ",over", Toast.LENGTH_SHORT).show();
	
	}
	
}
