package com.xxt.day1;

public class Demo3_UseStudent {
	public static void main(String[] args){
		
		Student s1 = new Student();
		s1.rollno = "S005";   // public,所有类都可以访问
		s1.name   = "刘俊";   // 默认的,同一个包里的其他类可以访问
		//s1.age  = 40;         // private成员不能被其他类访问
		s1.gender = 'M';      // protected,同一个包里的其他类可以访问protected成员
	}
}
