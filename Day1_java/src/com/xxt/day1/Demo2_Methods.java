package com.xxt.day1;
//一个java文件里可以定义多个类
//但是只能有一个类是公开的
//如果一个文件里有多个类，文件名怎么定义：如果文件里没有public的类，文件名随便起
//但是如果有一个公开的类，那么文件名必须和public类的名字一样

class Calculator{

    //访问说明符 返回类型 函数名(参数){方法体}
    public void sayHi(){ //没有参数，没有返回类型的方法
        System.out.println("Hi");
    }
   
    public void sum(int a, int b){ //带参数的方法a和b为形式参数
        System.out.println(a + b);
    }

    public float add(float a, float b, float c){ //带返回值的方法
        return a + b + c;
    }
}

public class Demo2_Methods {
    public static void main(String[] args){
        Calculator cal = new Calculator();
        cal.sum(10, 20); //10和20实参

        float result = cal.add(10.5f, 2.3f, 5.5f);
        System.out.println(result);

    }
}
