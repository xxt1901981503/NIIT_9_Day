package com.xxt.day1;

public class Student {

    //学号、姓名、年龄、性别
    //public 公开的，类成员完全公开，说明任何类都可以访问该成员
    public String rollno; //成员变量
    //默认的，同一个包里的其他类是可以访问该成员
    String name;
    //private，私有的，归该类私有，那么只能被该类访问
    private int age;
    //受保护的，同一个包里的其他类都可以访问，不同包里只有子类可以访问
    protected char gender; //M,F
    
    //构造函数（Constructor）：方法名和类名相同，没有返回类型，主要执行初始化工作
    public Student(){
        rollno = "S001";
        name   = "阿力";
        age    = 18;
        gender = 'F';
    }
    
    public Student(String r, String n){
        rollno = r;
        name = n;
    }

    public Student(String r, String n, int a, char g){
        rollno = r;
        name = n;
        age = a;
        gender = g;
    }

    //方法
    //访问说明符 返回类型 函数名(参数列表) {方法体}
    //成员方法
    public void takeExam(){
        System.out.println("逢考比100!");
    }

    public void displayDetails(){
        System.out.println("学号：" + rollno);
        System.out.println("姓名：" + name);
        System.out.println("年龄：" + age);
        System.out.println("性别：" + gender);
    }

    public static void main(String[] args){
        //对象
        //类名 对象名 = new 类名();
        //ALT + SHIFT + R 快速修改变量名
        Student s1 = new Student();	//构造函数创建对象时自动执行
        //访问类成员，对象名.成员名
        //ALT + SHIFT+ R 快速修改变量名
        s1.takeExam();
        s1.displayDetails();
        
        //Ctrl + / 添加和撤销注释
        Student s2;
        s2 = new Student("S002","Anna");
        s2.displayDetails();

        Student s3 = new Student("S003", "Anna2", 18, 'F');
        s3.displayDetails();

    }
}
