package com.xxt.day1;
//类成员：变量和方法，内部类
public class Demo1_Test {
    
    int age = 10; //全局变量，类里面定义的变量

    public void sayHi(){
        System.out.println("Hello Anna");
        //alt + /
    }

    //程序执行的入口，main方法
    public static void main(String[] args){
        //System.out.println("Hello Anna"); 
        /*
         *多行注释
         */

        //变量定义 数据类型 变量名
        int a = 10; //局部变量，方法里面定义的变量
        int b;
        b = 20;
        int c = 1, d = 2, e = 3;

        System.out.println(a);
        //数据类型
        //java的8个基本数据类型：
        //四个整数类型：byte(1)，short(2)，int(4)，long(8)
        //两个浮点类型：float(4)，double(8)
        //布尔类型：boolean(1)
        //字符类型：char(2)

        byte f = -128;
        float g = 10.5f;
        double h = 20.5; //小数默认是double类型

        boolean flag = true; //true false
        char gender = 'M';

        //String字符串
        String name = "Anna";
    }
}
