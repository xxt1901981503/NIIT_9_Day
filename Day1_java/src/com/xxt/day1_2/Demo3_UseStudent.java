package com.xxt.day1_2;

import com.xxt.day1.Student;

//快速修复导入：Ctrl + Shift + o

public class Demo3_UseStudent {
	public static void main(String[] args){
		
		Student s1 = new Student();
		s1.rollno = "S005";		//public,所有类都可以访问
		//s1.name   = "刘俊";		//默认的,同一个包里的其他类可以访问
		//s1.age  = 40;			//private成员不能被其他类访问
		//s1.gender = 'M';		//protected,同一个包里的其他类可以访问protected成员

		//public，默认的，protected， private
		//public > protected > 默认的 > private
		//那几个可以修饰类：public，默认的
	}
}
