package com.xxt.android_day3_dialandsend;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	
	Button dial, call, send;
	EditText et_number, et_msg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		dial = (Button)findViewById(R.id.btn_dial);
		call = (Button)findViewById(R.id.btn_call);
		send = (Button)findViewById(R.id.btn_send);
		
		et_number = (EditText)findViewById(R.id.et_number);
		et_msg = (EditText)findViewById(R.id.et_msg);
		
		//注册监听器
		dial.setOnClickListener(this);
		call.setOnClickListener(this);
		send.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btn_dial: 
			Intent i1 = new Intent();
			i1.setAction(Intent.ACTION_DIAL);
			//i1.setData(Uri.parse("tel:13697665261"));//设置下数据，这里给出的是要拨打的电话号码
			i1.setData(Uri.parse("tel:" + et_number.getText().toString()));//设置下数据，这里给出的是要拨打的电话号码
			startActivity(i1);
			break;
		case R.id.btn_call:
			Intent i2 = new Intent();
			i2.setAction(Intent.ACTION_CALL);
			i2.setData(Uri.parse("tel:" + et_number.getText().toString()));
			startActivity(i2);
			break;
		case R.id.btn_send:
			String number = et_number.getText().toString();
			String msg = et_msg.getText().toString();
			SmsManager sm = SmsManager.getDefault();
			//第一个参数：发送短信的目标号码
			//第二个参数：短信转发中心号码，null表示默认
			//第三个参数：要发送的短信内容
			//第四个参数：确认短信是否发出成功要使用PendingIntent对象
			//第五个参数：确认收到短信成功要使用的PendingIntent对象
			sm.sendTextMessage(number, null, msg, null, null);
			Toast.makeText(this, "短信发送成功！", Toast.LENGTH_SHORT).show();
			
			break;
		
		}
	}
	
	
	
}
