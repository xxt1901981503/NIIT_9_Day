package com.xxt.android_day2_eventhandling;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnLongClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {
	
	Button b1, b2;
	EditText et1;
	//Ctrl + Shift + o
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		b1 = (Button)findViewById(R.id.button1);
		b2 = (Button)findViewById(R.id.button2);
		et1 = (EditText)findViewById(R.id.editText1);
		
		ButtonListener listener = new ButtonListener();
		b1.setOnClickListener(listener);
		//b2.setOnClickListener(new ButtonListener());
		b2.setOnLongClickListener(new OnLongClickListener() {
			
			@Override
			public boolean onLongClick(View v) {
				//makeText的三个参数
				//1.上下文，传当前活动对象即可
				//2.字符串，要显示的消息
				//3.一个整数，表示显示的时间长短
				Toast.makeText(MainActivity.this, "啊，按钮2被长按了", Toast.LENGTH_SHORT).show();
				
				return true;//返回true是说明事件这里已经处理掉了，不需要继续传播
			}
		});
		
		et1.setOnFocusChangeListener(new OnFocusChangeListener() {
			
			@Override
			public void onFocusChange(View v, boolean hasFocus) {
				if(!hasFocus && TextUtils.isEmpty(et1.getText().toString())){
					Toast.makeText(MainActivity.this, "请输入用户名", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	class ButtonListener implements OnClickListener{

		@Override
		public void onClick(View v) {//事件源对象
			//b1.setBackgroundColor(Color.YELLOW);
			v.setBackgroundColor(Color.GREEN);
		}

		
	}

}
