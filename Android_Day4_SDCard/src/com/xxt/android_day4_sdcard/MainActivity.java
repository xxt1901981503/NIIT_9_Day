package com.xxt.android_day4_sdcard;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import android.app.Activity;
import android.content.ContentValues;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener{

	Button read, write, clear;
	TextView tvDisplay;
	EditText etFilename, etContent;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViews();
	}

	public void findViews() {
		// 查找按钮
		read = (Button) findViewById(R.id.btnRead);
		write = (Button) findViewById(R.id.btnWrite);
		clear = (Button) findViewById(R.id.btnClear);

		// 查找TextView
		tvDisplay = (TextView) findViewById(R.id.tvDisplay);

		// 查找文本框
		etFilename = (EditText) findViewById(R.id.etFile);
		etContent = (EditText) findViewById(R.id.etContent);
		
		write.setOnClickListener(this);
		read.setOnClickListener(this);
		clear.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btnWrite:
			writeToFile(etFilename.getText().toString(),etContent.getText().toString());
			//获取用户输入的文件名和文件内容，写入到文件
			break;
		case R.id.btnRead:
			readFromFile(etFilename.getText().toString());
			//自定义一个方法，传入文件名，读取文件内容显示在textview上
			break;
		case R.id.btnClear:
			etFilename.setText("");
			etContent.setText("");
			break;
		}
	}
	
	//OutputStream 输出流 写入 FileOutStream
	//InputStream 输入流 读取 FileInputStream
	
	public void readFromFile(String filename) {
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			
			String path = Environment.getExternalStorageDirectory() + "/" + filename;
			try{
				
				FileInputStream fis = new FileInputStream(path);
				byte[] data = new byte[fis.available()];//fis.available判断流中可读字节数
				fis.read(data);//读取数据到data数组
				fis.close();
				String text = new String(data);
				tvDisplay.setText(text);
				
			}catch(Exception e){
				Log.e("MainActivity", e.getMessage());
			}
		}
	
	}

	public void writeToFile(String filename, String content) {
		// 获取外部设备的状态
		String state = Environment.getExternalStorageState();
		if (state.equals(Environment.MEDIA_MOUNTED)) {
			// /mnt/sdcard/anna.txt
			String path = Environment.getExternalStorageDirectory() + "/" + filename;

			// 参数1：要写入的文件的路径
			// 参数2：是否使用追加模式 每次新写入的内容追加到文件末尾
			try {

				FileOutputStream fos = new FileOutputStream(path, true);
				fos.write(content.getBytes());//把內容转成一个字节数组写入
				fos.close();	//关闭流
				Toast.makeText(this, "写入成功", Toast.LENGTH_SHORT).show();
				
				//<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
				
			} catch (Exception e) {
				Log.e("MainActivity", e.getMessage());
			}

		}
	}
	
}
