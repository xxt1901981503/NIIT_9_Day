package com.niit.receiver2;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MessageReceiver2 extends BroadcastReceiver{

	@Override
	public void onReceive(Context context, Intent intent) {
		Toast.makeText(context,"Receiver2收到消息,呵呵，我拦截了！",Toast.LENGTH_SHORT).show();
		abortBroadcast();
		//若不需要继续向下一个广播接收器传送该广播，则可以调用abortBroadcast()终止继续传播
	}
}
