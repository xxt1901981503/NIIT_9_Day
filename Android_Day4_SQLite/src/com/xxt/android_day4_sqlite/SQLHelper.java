package com.xxt.android_day4_sqlite;
//SQLiteOpenHelper类实现数据库的创建和升级

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;

public class SQLHelper extends SQLiteOpenHelper{
	
	//参数1：使用SQLHelper的上下文
	//参数2：数据库文件的名称
	//参数3：游标工厂对象，如何生成结果集，null表示使用默认的
	//参数4：数据库的版本，从1开始即可，一个整数
	public SQLHelper(Context context) {
		super(context, "NIIT.db", null, 1);
		
	}

	//当数据库被创建的时候被执行，主要在这里执行创建表和插入初始记录
	@Override
	public void onCreate(SQLiteDatabase db) {
		//SQLite数据类型，integer，text，null，real，blob
		db.execSQL("create table Contacts(id integer primary key autoincrement, name text, mobile text)");
	}
	
	//当数据库升级的时候被调用，主要在此执行表的删除、修改
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
	}
	
}
