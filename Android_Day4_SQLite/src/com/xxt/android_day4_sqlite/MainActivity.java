package com.xxt.android_day4_sqlite;

import android.app.Activity;
import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	
	Button btn_insert, btn_modify, btn_delete, btn_queryOne, btn_queryAll;
	EditText et_id, et_name, et_mobile;
	//1.创建数据库和表
	//2.实现数据库的插入、更改和删除
	SQLHelper helper;	//实现数据的创建和升级
	SQLiteDatabase db;	//SQLiteDatabase类公开了方法，实现增删改查
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViews();
		helper = new SQLHelper(this);		//实例化了Helper类
		db = helper.getWritableDatabase();	
		//创建或打开数据库
		//获取数据库实例，这里获取之后可向数据库进行读写
		//数据库是在第一次使用数据库的时候创建，也就是第一次调用getWritableDatabase()或
		//getReadableDataBase()方法的时候调用
		
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		db.close(); //关闭数据库，释放资源
	}



	//查找视图和注册监听器
	public void findViews(){
		btn_insert = (Button)findViewById(R.id.btn_insert);
		btn_modify = (Button)findViewById(R.id.btn_modify);
		btn_delete = (Button)findViewById(R.id.btn_delete);
		btn_queryOne = (Button)findViewById(R.id.btn_queryOne);
		btn_queryAll = (Button)findViewById(R.id.btn_queryAll);
		
		et_id = (EditText)findViewById(R.id.et_Id);
		et_name = (EditText)findViewById(R.id.et_Name);
		et_mobile = (EditText)findViewById(R.id.et_Mobile);
	
		btn_insert.setOnClickListener(this);
		btn_modify.setOnClickListener(this);
		btn_delete.setOnClickListener(this);
		btn_queryOne.setOnClickListener(this);
		btn_queryAll.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch(v.getId()){
		case R.id.btn_insert:
			String name = et_name.getText().toString();
			String mobile = et_mobile.getText().toString();
			//参数1：表名
			//参数2：需要插入null值的列名
			//参数3：一个ContenValues对象，表示一行对象
			
			//创建一个行对象
			ContentValues newrow = new ContentValues();
			newrow.put("name", name);//指定要插入哪个列，插入什么值
			newrow.put("mobile", mobile);
			
			db.insert("Contacts", null, newrow);
			Toast.makeText(this, "插入成功", Toast.LENGTH_SHORT).show();
			break;
		case R.id.btn_modify:
			//创建一个行对象，设置更新后的值
			ContentValues updatedRow = new ContentValues();
			updatedRow.put("name", et_name.getText().toString());
			updatedRow.put("mobile", et_mobile.getText().toString());
			String[] args2 = {et_id.getText().toString()};
			//参数1：表名
			//参数2：更新后的行的数据
			//参数3：where条件，指定要更新哪一行
			//惨数4：where条件参数的值，用一个字符串数组表示
			db.update("contacts", updatedRow, "id=?", args2);
			Toast.makeText(this, "更新成功", Toast.LENGTH_SHORT).show();
			break;
		case R.id.btn_delete:
			String [] args3 = {et_id.getText().toString()};
			db.delete("Contacts", "id=?", args3);
			Toast.makeText(this, "更新成功", Toast.LENGTH_SHORT).show();
			break;
		case R.id.btn_queryOne:
			//参数1：要执行的查询语句
			//参数2：参数列表，一个字符串数组
			//String[] args1 = {et_id.getText().toString(),et_name.getText().toString()};
			String[] args1 = {et_id.getText().toString()};
			//Cursor cursor = db.rawQuery("select * from Contacts where id=? and name =?", args1);
			
			//查询后返回一个Cursor对象，用于访问结果集
			//这里我们用*好，索引查询的是所有列，总共三列，列索引分别是0,1,2
			Cursor cursor = db.rawQuery("select * from Contacts where id=?", args1);
			
			if(cursor.moveToFirst()){//判断有没有第一行，若有，移动到第一行
				
				et_name.setText(cursor.getString(1));
				//因为name列是字符串类型，所以用getString，1表示第二列，列索引从0刚开始的
				et_mobile.setText(cursor.getString(2));
				//因为mobile列是字符串类型，所以用getString，2表示第三列，列索引从0刚开始的
			
			} else {
				et_name.setText("");
				et_mobile.setText("");
				Toast.makeText(this, "查无此人", Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.btn_queryAll:
			Cursor cc = db.rawQuery("select * from Contacts", null);
			
			//如果你一个字符串需要不但更改或附加内容，那么请用StringBuilder类
			StringBuilder sb = new StringBuilder();
			
			while(cc.moveToNext()){//判断有没有下一条记录，并且移动到下一条记录
				sb.append(cc.getInt(0));//追加
				sb.append(",");
				sb.append(cc.getString(1));
				sb.append(",");
				sb.append(cc.getString(2));
				sb.append("\n");
			}
			
			Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();
			
			break;
		}
	}
	
}
