package com.xxt.android_day2_layout;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends Activity {
	
	public static final String TAG = "MainActivity";
	
	//生命周期onCreate方法，当活动被创建的时候调用，而且只调用一次，主要做一些全局初始化的工作
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.layout_linear1);
		Log.i(TAG, "---onCreate---");//information提示性消息 绿色
		//Log.e(tag, msg); //在日志窗口输出错误消息error 红色
		//Log.w(tag, msg); //警告信息warning橙色
		//Log.d(tag, msg); //调试消息debugging 蓝色
		//Log.v(tag, msg); //详细信息verbose黑色
	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.i(TAG, "---onStart---");
	}

	@Override
	protected void onRestart() {
		super.onRestart();
		Log.i(TAG, "---onRestart---");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.i(TAG, "---onResume---");
	}

	@Override
	protected void onPause() {
		super.onPause();
		Log.i(TAG, "---onPause---");
	}

	@Override
	protected void onStop() {
		super.onStop();
		Log.i(TAG, "---onStop---");
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "---onDestroy---");
	}
	
}
