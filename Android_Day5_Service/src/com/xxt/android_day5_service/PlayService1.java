package com.xxt.android_day5_service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

public class PlayService1 extends Service{

	public static final String TAG = "PlayService1";
	MediaPlayer mp;//媒体播放器对象
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}
	
	//服务被创建的时候执行，那么执行初始化的工作
	@Override
	public void onCreate() {
		super.onCreate();
		Log.i(TAG, "--onCreate--");
		mp = MediaPlayer.create(this, R.raw.music1);
		mp.setLooping(true);//单曲循环
	}
	
	//服务启动时被执行，一般用于启动你的服务
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.i(TAG, "--onStartCommand--");
		mp.start();
		return super.onStartCommand(intent, flags, startId);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		Log.i(TAG, "--onDestroy--");
		mp.stop();
	}
	
}
