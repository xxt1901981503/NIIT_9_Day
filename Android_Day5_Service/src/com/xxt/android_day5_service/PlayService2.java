package com.xxt.android_day5_service;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.IBinder;
import android.util.Log;

public class PlayService2 extends Service{

	public static final String TAG = "PlayService2";
	MediaPlayer mp;
	
	//当服务被绑定的时候被执行
	@Override
	public IBinder onBind(Intent intent) {
		Log.i(TAG,"--onBind--");
		mp.start();
		return null;
		//返回一个IBinder对象,用于实现绑定的服务和被绑定的组件进行通信
		//IBinder是个接口，所以你需要实现该接口或继承Binder类派生一个IBinder类
		//然后返回它的对象
	}

	@Override
	public void onCreate() {
		super.onCreate();
		Log.i(TAG, "--onCreate--");
		mp = MediaPlayer.create(this, R.raw.music2);
		mp.setLooping(true);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mp.stop();
		Log.i(TAG, "--onDestroy--");
	}

	@Override
	public boolean onUnbind(Intent intent) {
		Log.i(TAG, "--onUnbind--");
		return super.onUnbind(intent);
	}

}
