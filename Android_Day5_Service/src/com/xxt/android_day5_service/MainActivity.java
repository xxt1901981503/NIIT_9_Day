package com.xxt.android_day5_service;

import android.app.Activity;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener {

	Button start, stop, bind, unbind;
	Intent i1, i2;
	PlayServiceConnection con;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		findViews();
		i1 = new Intent(MainActivity.this,PlayService1.class);
		i2 = new Intent(MainActivity.this,PlayService2.class);
		con = new PlayServiceConnection();
	}

	public void findViews() {

		start = (Button) findViewById(R.id.btn_start);
		stop = (Button) findViewById(R.id.btn_stop);
		bind = (Button) findViewById(R.id.btn_bind);
		unbind = (Button) findViewById(R.id.btn_unbind);

		start.setOnClickListener(this);
		stop.setOnClickListener(this);
		bind.setOnClickListener(this);
		unbind.setOnClickListener(this);
	}

	class PlayServiceConnection implements ServiceConnection{

		@Override
		public void onServiceConnected(ComponentName name, IBinder service) {
			
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			
		}
		
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_start:
			startService(i1);
			break;
		case R.id.btn_stop:
			stopService(i1);
			break;
		case R.id.btn_bind:
			//参数1：意图
			//参数2：一个ServiceConnection对象，监视服务连接状态
			//参数3：一个整数标记值，表示服务创建的额外信息
			bindService(i2, con, Service.BIND_AUTO_CREATE);
			break;
		case R.id.btn_unbind:
			unbindService(con);//需要传入监视服务连接状态的对象
			break;
		}
	}
}
