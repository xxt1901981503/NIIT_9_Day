package com.xxt.android_day2_intent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {

	Button b;
	EditText et_name, et_title;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		b = (Button)findViewById(R.id.main_btn1);
		et_name = (EditText)findViewById(R.id.et_name);
		et_title = (EditText)findViewById(R.id.et_title);
		
		b.setOnClickListener(new OnClickListener(){
		
			@Override
			public void onClick(View v) {
                //显示Intent，明确指定要请的组件
				Intent intent = new Intent(MainActivity.this, SecondActivity.class);
				
				Bundle bundle = new Bundle(); //创建一个束对象，相当于一二数据包
				bundle.putString("name", et_name.getText().toString());
				bundle.putString("title", et_title.getText().toString());
				bundle.putInt("age", 18);
				intent.putExtras(bundle);
				
				startActivity(intent);
			}
					
		});
	}

}
