package com.xxt.android_day2_intent;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SecondActivity extends Activity{
	
	Button back, go;
	TextView tv_msg;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_second);
				
		back = (Button)findViewById(R.id.second_btn1);
		go = (Button)findViewById(R.id.second_btn2);
		tv_msg = (TextView)findViewById(R.id.tv_msg);
		
		//获取启动当前活动的Intent对象
		Intent intent = getIntent();
		//获取Intent对象中的Bundle数据包
		Bundle bundle = intent.getExtras();
		
		//根据键获取值
		String name = bundle.getString("name");
		String title = bundle.getString("title");
		int age = bundle.getInt("age");
		
		tv_msg.setText("你好，" + name + title + "，您" + age + "岁了");
		
		back.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				
				//Intent intent1 = new Intent(SecondActivity.this, MainActivity.class);
				//startActivity(intent1);
				
				SecondActivity.this.finish();
			}
		});
		
		go.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				//显示的Intent,明确指定了要启动的目标组价，一般用于启动同程序内的组件
				//隐式的Intent，不明确指定要启动的目标组件，通过筛选条件启动组件，一般用来启动跨程序的组件
				//Intent intent2 = new Intent(SecondActivity.this, MainActivity.class);
				
				//----------------------------------------
				
				Intent intent2 = new Intent();
				//设置要启动的组件的Action名称
				intent2.setAction("com.xxt.intent.third");
				
				startActivity(intent2);
			}
		});
		
	}
	
}
