package com.xxt.day3;

import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

//JFrame 窗体类
//JButton 按钮类
//java.lang String类

public class Demo5_MyFrame extends JFrame {

    JButton b1, b2;

    public Demo5_MyFrame(){
        this.setTitle("窗体的标题"); //this代指当前对象
        this.setSize(400, 500);
        this.setLocation(100, 200);
        this.setDefaultCloseOperation(3); //默认关闭方式，3表示关闭窗体则退出程序
        this.setLayout(new FlowLayout());

        b1 = new JButton("点我");
        b2 = new JButton("点点我");

        this.add(b1);
        this.add(b2);

        this.setVisible(true);

        //点击事件 = 动作事件 Action Event
        //1.事件源 按钮
        //2.监听器
        //3.事件处理程序 event handler 响应事件的发生被执行的方法
        ButtonListener listener = new ButtonListener();
        b1.addActionListener(listener); //注册监听器

        b2.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent e) {
				b2.setBackground(Color.GREEN);
			}

        });
    }
    
    //ActionListener
    class ButtonListener implements ActionListener{

        //事件处理程序
		@Override
		public void actionPerformed(ActionEvent arg0) {
			b1.setBackground(Color.RED);
		}
        
    }

	public static void main(String[] args) {
		Demo5_MyFrame frame = new Demo5_MyFrame();
	}
}
