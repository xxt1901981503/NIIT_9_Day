package com.xxt.day3;

//参数传递：按值传递和按引用专递

class Person{
    String name;
}

class Student extends Person{

}

class Teacher extends Person{

}

class GoodStudent extends Person{

}

public class Demo4_PassParameters {
	
    public static void changeValue(int x){
        x = 10;
    }
    
    //如果说参数类型是一个类，那么需要传该类的对象
    //如果方法的参数是一个普通类，可以传入该类的对象或该类派生类的对象
    //如果方法的参数是一个抽象类，可以传入该非抽象的派生类的对象
    //如果方法的参数是一个接口，可以传入实现了该接口的非抽象类的对象
    public static void changeName(Person person){
        person.name = "小强";    
    }
    
	public static void main(String[] args) {
		int a = 100;
        changeValue(a); //按值传递:参数的类型为基本数据类型（8）
        System.out.println(a);

        Person p = new Person();
        p.name = "李柏杨";
        changeName(p); //按引用传递：数组，类对象
        System.out.println(p.name);

        Student s = new Student();
        s.name = "赵有力";
        changeName(s);
        System.out.println(s.name);

        Teacher t = new Teacher();
        t.name = "Anna";
        changeName(t);
        System.out.println(t.name);

        GoodStudent gs = new GoodStudent();
        gs.name = "彭朝领";
        changeName(gs);
        System.out.println(gs.name);

	}
}
