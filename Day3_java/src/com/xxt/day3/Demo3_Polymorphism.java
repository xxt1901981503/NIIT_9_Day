package com.xxt.day3;

//多态，一个实体，多种形态，主要说方法的多态
//1.方法重写/方法覆盖：一个方法在父类和子类里是不同的形态
//2.方法重载（method overloading）：同一个类里，同一个方法，多种形态
//方法重载：同一个类里，同名的方法可定义多次，单参数要不同
//1.参数个数不同
//2.参数类型不同
//3.参数顺序不同

class Calculator{

    public static void sum(int a, int b){
        System.out.println( a + b );
    }

    public static void sum(int a, int b, int c){
        System.out.println(a + b + c);
    }

    public static void sum(float a, float b){
        System.out.println(a + b);
    }
    
    public static void sum(int a, float b){
        System.out.println(a + b);
    }
    
    public static void sum(float b, int a){
        System.out.println(a + b);
    }

    //public static float sum(float a, int b){}
}

public class Demo3_Polymorphism {
	public static void main(String[] args) {
		Calculator.sum(10, 20);      // 1
		Calculator.sum(10.2f, 2.5f); // 3
		Calculator.sum(10.5f, 20);   // 5
		Calculator.sum(20, 2.6f);    // 4
		Calculator.sum(10, 20, 30);  // 2
	}
}
