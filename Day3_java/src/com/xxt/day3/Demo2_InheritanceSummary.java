package com.xxt.day3;

//所有的类都已经继承自Object类，Object类是所有类的基类（base class）
//基类和父类，父类就是直接的父类，基类指该类从哪个类派生
//A->B->C B是C的父类，A是C的基类

class A{
    int a;
    void methodA(){};
}

class B{
    int b;
    void methodB(){};
}

//C类有3个成员
class C extends A{
    int c;
}

//类不支持多重继承，一个类不能同时有两个父类
//class D extends A, B{
//
//}

interface E{
    int e = 10;
    void methodE();
}

interface F{
    void methodF(){};	//Abstract methods do not specify a body
}

//类可以继承自类，接口可以继承自接口
interface G extends E{
    void methodG();

}

//java支持接口的多重继承
interface H extends E, F{
    
}

//类I继承自类A，并且同时实现接口E和F，继承和实现可以同时发生
class I extends A implements E, F{

	@Override
	public void methodF() {
		
	}

	@Override
	public void methodE() {
		
	}

}

class Animal{
    String color;
    int leg;
}

interface 陆栖动物行为{
    void crawl();
}

interface 水栖动物行为{
    void swim();
}

class Frog extends Animal implements 陆栖动物行为,水栖动物行为{

	@Override
	public void swim() {
		
	}

	@Override
	public void crawl() {
		
	}
    
}

//普通类->半抽象的类(抽象类)->完全抽象的类（接口）

