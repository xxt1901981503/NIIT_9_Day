package com.xxt.day3;

import java.util.HashMap;
import java.util.Set;

//Map集合类，以键-值对的形式存放数据，根据键查找值，键不可重复，值可以重复
public class Demo8_HashMap {
	public static void main(String[] args) {
		HashMap<Integer, String> hm = new HashMap<Integer, String>();
        hm.put(1, "China");                            // 键，值
        hm.put(2, "America");
        hm.put(3, "Korea");
        hm.put(4, "Japan");
        System.out.println(hm);
        System.out.println(hm.size());
        System.out.println(hm.get(1));
        System.out.println(hm.get(4));                 // 根据键获取值

        System.out.println(hm.isEmpty());              // false
        System.out.println(hm.containsKey(2));         // 是否包含指定的键true
        System.out.println(hm.containsValue("China")); // 是否包含指定的值true
        hm.remove(4);                                  // 根据键移除元素对
        //hm.clear();                                  //清空集合

        //遍历集合，打印键和对应的值
        Set<Integer> set = hm.keySet();
        for(Integer key:set){
            System.out.println(key + "-" + hm.get(key));
        }

	}
}
