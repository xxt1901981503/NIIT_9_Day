package com.xxt.day3;

import java.util.HashSet;

//集合：Collection 动态数组，类型不固定，大小不固定，可以动态增删改
//数组：大小固定，不能动态增删，类型固定，增删不方便
//Set集合，List集合，Map集合
//HashSet集合类，ArrayList集合类，HashMap集合类
//HashSet集合类：不允许重复，无序，方便快速插入，但是不方便检索

public class Demo6_HashSet {
	public static void main(String[] args) {
        
        //byte,short,int,long,float,double,boolean,char
        //封装器类
        //Byte,Short,Integer,Long,Float,Double,Boolean,Character

        HashSet<String> hs = new HashSet<String>();
        hs.add("zhaoyouli");                     // add(Object c)
        hs.add("Anna");
        hs.add("Liboyang");
        hs.add("Shangliushuai");
        //hs.add(10);                            // new Integer(10)
        //hs.add(10.5f);                         // new Float(10.5f)
        hs.add("Anna");
        
        System.out.println(hs);                  // hs.toString()
        System.out.println(hs.size());           // 集合的大小
        System.out.println(hs.isEmpty());        // 集合是否为空
        System.out.println(hs.contains("Anna")); // 判断集合中是否有指定的元素
        hs.remove("Shangliushuai");              // 移除指定的元素
        //hs.clear();                            // 清空集合

        //遍历集合
        for(String x:hs){
            System.out.print(x + " ");
        }
	}
}
