package com.xxt.day3;

//interface:完全抽象的类
//接口里面的方法默认全部是抽象方法而且是public
//接口里面的变量默认全部是final,而且是static

interface Shape{

    double PI = 3.14;

    void calArea();

    void calZhouchang();
}

//implements实现 类实现接口
class Circle implements Shape{
	
	double radius;

	public Circle(double radius) {
		this.radius = radius;
	}
	
    public void calArea(){
        System.out.println(PI * radius * radius);       
    }
	
    public void calZhouchang(){
        System.out.println(2 * PI * radius);
    }
}

class Rectangle implements Shape{
    
    double width;
    double height;

    public Rectangle(double width, double height){
        this.width = width;
        this.height = height;
    }

	@Override
	public void calArea() {
		System.out.println(width * height);
	}

	@Override
	public void calZhouchang() {
		System.out.println(2 * (width + height));
	}
}
public class Demo1_Interface {
	public static void main(String[] args) {
        System.out.println(Shape.PI);
        //Shape shape = new Shape();  //接口不能实例化，接口不能有构造函数
        //抽象类可以有构造函数，可以给静态变量赋值
        Circle c1 = new Circle(4);
        c1.calArea();
        c1.calZhouchang();

        Rectangle rect = new Rectangle(20, 10);
        rect.calArea();
        rect.calZhouchang();

        Shape s = c1;
        s.calArea();

        s = rect;
        s.calArea();

        //创建一个对象ss，创建的是一个匿名类的对象，该对象实现Shape接口
        Shape ss = new Shape(){

			@Override
			public void calArea() {
				
			}

			@Override
			public void calZhouchang() {
				
			}
        };
        
        //创建了一个对象cc，该对象是一个匿名类的对象，该匿名类派生自Circle类，也就是继承自Circle类
        Circle cc = new Circle(2){
            
        };

        //Ctrl + / 添加和取消单行注释
        //Ctrl + Shift + / 添加和取消多行注释
        //Ctrl + Shift + \取消多行注释

        //假设Student是个普通类，Teacher是个抽象类，Shape是个接口
        //Student a = new Student(){};
        //Teacher b = new Teacher(){};
        //Shape c = new Shape(){};
        //a对象是一个继承自Student类的匿名类的对象
        //b对象是一个继承自Teacher类的匿名类的对象
	}
}
