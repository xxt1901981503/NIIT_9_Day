package com.xxt.day3;

import java.util.ArrayList;

//ArrayList集合的特点：有序，允许重复，检索迅速
//HashSet集合的特点：无序，不允许重复，插入迅速，检索麻烦

public class Demo7_ArrayList {
	public static void main(String[] args) {

	    ArrayList<String> list = new ArrayList<String>();
        list.add("Anna");
        list.add("Zhaoyouli");
        list.add("Jenny");
        list.add("Anny");

        System.out.println(list);
        System.out.println(list.size());
        System.out.println(list.isEmpty());
        System.out.println(list.contains("Anna"));
        
        System.out.println(list.get(0)); //获取指定位置的元素
        list.remove("Zhaoyouli");
        list.remove(0); //移除指定位置的元素
        //list.clear();

        for(String x:list){
            System.out.print(x + " ");
        }

        ArrayList<Integer> list2 = new ArrayList<Integer>();
        list2.add(10);
        list2.add(12);
        list2.add(20);

        for(Integer y:list2){
            System.out.print(y + " ");
        }
	}
}
